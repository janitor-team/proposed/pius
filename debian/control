Source: pius
Section: utils
Priority: optional
Maintainer: Felix Lechner <felix.lechner@lease-up.com>
Build-Depends:
 debhelper-compat (= 12),
 python3-all,
 dh-python
Standards-Version: 4.4.1
Rules-Requires-Root: no
Homepage: http://www.phildev.net/pius/
Vcs-Browser: https://salsa.debian.org/lechner-guest/pius
Vcs-Git: https://salsa.debian.org/lechner-guest/pius.git

Package: pius
Architecture: all
Depends: gnupg (>= 2), ${misc:Depends}, ${python3:Depends}
Description: Tools to help before and after key-signing parties
 After a key-signing party, pius (the PGP Individual UID Signer) signs each
 uid on a GPG key individually. Each signature is encrypted and mailed to
 the email address associated with that particular uid. As a result of this
 process, the recipient can choose which signatures to import. Also,
 signatures of inactive uids are not delivered. This tool greatly reduces
 time and error when signing keys.
 .
 Other tools herein are useful for organizers: pius-keyring-mgr builds a party
 keyring from a CSV file or by scanning mailboxes, and pius-party-worksheet
 generates a worksheet as a hand out. If someone has not signed your key,
 pius-report can analyze a party keyring and remind them.
 .
 This version supports GPGv2 and uses it by default.
